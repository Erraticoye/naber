# -*- coding: utf-8 -*-
import logging
import socket
from threading import Thread


logging.basicConfig(filename=f"mesajlaşma.log",
                    format='%(asctime)s %(message)s',
                    filemode='w')

logger = logging.getLogger()

logger.setLevel(logging.DEBUG)

hostname = socket.gethostname()
SERVER_HOST = socket.gethostbyname(hostname)
def port():
    global PORT
    PORT = input("Dinlenmesini istediğiniz port: ")


port()
if PORT.isnumeric():
    SERVER_PORT = int(PORT)
    logger.debug(f"{PORT} portu kullanım için seçildi")
else:
    print("Bak kardeş port nedir biliyormusun bilmiyorsan Google'a sorbilirsin. Lütfen sayısal bir veri girn")
    port()
separator_token = "<SEP>"

client_sockets = set()
s = socket.socket()

s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

s.bind((SERVER_HOST, SERVER_PORT))
s.listen(5)
print(f"[*] {SERVER_HOST}:{SERVER_PORT} adresine gelen istekler dinleniyor...")
logger.debug(f"[*] {SERVER_HOST}:{SERVER_PORT} adresine gelen istekler dinleniyor...")


def mesajları_dinle(cs):
    while True:
        try:
            msg = cs.recv(1024).decode()
        except Exception as e:
            print(f"[!] Bir hata meydana geldi: {e}")
            logger.debug(f"[!] Bir hata meydana geldi: {e}")
            client_sockets.remove(cs)
            logger.debug(f"Soketler kapatıldı")
        else:
            msg = msg.replace(separator_token, ": ")
            if msg != "":
                print(msg)
                logger.debug(msg)
        for client_socket in client_sockets:
            client_socket.send(msg.encode())


while True:
    client_socket, client_address = s.accept()
    print(f"[+] {client_address} Bağlandı.")
    logger.debug(f"[+] {client_address} Bağlanıyor.")
    client_sockets.add(client_socket)
    logger.debug(f"[+] {client_address} Bağlandı.")
    t = Thread(target=mesajları_dinle, args=(client_socket,))
    t.daemon = True
    t.start()

for cs in client_sockets:
    cs.close()
s.close()
