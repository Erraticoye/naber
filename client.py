# -*- coding: utf-8 -*-
import logging
import socket
import random
from threading import Thread
from datetime import datetime
from colorama import Fore, init

logging.basicConfig(filename=f"mesajlaşma.log",
                    format='%(asctime)s %(message)s',
                    filemode='w')

logger = logging.getLogger()

logger.setLevel(logging.DEBUG)

init()

renkler = [Fore.BLUE, Fore.CYAN, Fore.GREEN, Fore.LIGHTBLACK_EX,
           Fore.LIGHTBLUE_EX, Fore.LIGHTCYAN_EX, Fore.LIGHTGREEN_EX,
           Fore.LIGHTMAGENTA_EX, Fore.LIGHTRED_EX, Fore.LIGHTWHITE_EX,
           Fore.LIGHTYELLOW_EX, Fore.MAGENTA, Fore.RED, Fore.WHITE, Fore.YELLOW
           ]

client_color = random.choice(renkler)

SERVER_HOST = input("Sunucu ip adresi: ")
logger.debug(f"Sunucu ip adresi {SERVER_HOST} olarak belirlendi.")
SERVER_PORT = input("Sunucu portu: ")
logger.debug(f"Sunucu portu {SERVER_PORT} olarak belirlendi.")
separator_token = "<SEP>"

s = socket.socket()
print(f"[*] {SERVER_HOST}:{SERVER_PORT} Sunucusuna Bağlanılıyor...")
logger.debug(f"[*] {SERVER_HOST}:{SERVER_PORT} Sunucusuna Bağlanılıyor...")

s.connect((SERVER_HOST, SERVER_PORT))
print("[+] Bağlanıldı.")
logger.debug("[+] Bağlanıldı.")

isim = input("Adınızı girin: ")


def mesajları_dinle():
    while True:
        mesaj = s.recv(1024).decode()
        print("\n" + mesaj)


t = Thread(target=mesajları_dinle)
t.daemon = True
t.start()

while True:
    mesaj = input()
    if mesaj.lower() == 'q':
        break
    date_now = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    mesaj = f"{client_color}[{date_now}] {isim}{separator_token}{mesaj}{Fore.RESET}"
    s.send(mesaj.encode())

s.close()
