## Naber v1.0
Merkeziyetsiz mesjlaşma ugulaması.
Naber'i kullanbilmek için minimum 2 adet cihaza sahip olmanız gerekmektedir ama 1 cihaz sunucu görevi göreceği için sunucu olarak kullanılacağı için sadece mesajları okuyup kayıt alabilir mesaj yazamaz. Bu yüzden en az 3 cihaz önerilir. Varsayılan ayarlarda 1 sunucuya aynı anda maksimum 5 kişi bağlanabilir. Bu ayar kaynak koddan değiştirilebilir.
## Planlanan Güncellemeler:

 1. Windows için derleme ve unix ve unix benzeri sistemler(macos, Linux, vb.) için çalıştırılma dizini ayarlama. (v1.5)
 2. Grafik Arayüz (v2.0)
 3. Biri çıkınca bilgilendirme (v2.1)
 4. Uygulama içerisinden kişi sınırını değiştirme (v2.1)

	Silebilirdim ama silmedim:
> Written with [StackEdit](https://stackedit.io/).
